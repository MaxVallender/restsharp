
# R e s t S h a r p

--------------------------------------------------------------------------------

## P o s t m a n

Postman besitzt einen Code-Export.
Dazu ganz weit rechts auf </> klicken.

Sieht etwas folgendermaßen aus:

	var client = new RestClient("baseURL/call?access_token=access_token");
	client.Timeout = -1;
	var request = new RestRequest(Method.Post);

	request.AddHeader("Accept", "application/json");
	request.AddHeader("Content-Type", "application/json");

	request.AddParameter("application/json", "{\r\n  \"KEY\": \"" + VALUE + "\"\r\n}", ParameterType.RequestBody);

	IRestResponse response = client.Execute(request);

	Console.WriteLine(response.Content);

--------------------------------------------------------------------------------

## V o r l a g e / F r a m e w o r k

Bisher wird noch ein access_token benötigt, welches noch hier noch nicht geholt wird!

	using RestSharp;
	//using RestSharp.Serialization.Json;

	...

	public class DataIn {
		public string something { get; set; }
	}

	public class DataOut {
		public string something { get; set; }
	}

	public class ApiCaller {
		RestClient client;

		ApiCaller() {
			client = new RestClient("baseUrl");
	// hole access_token
		}
		
		// baseUrl/call?access_token=access_token
		public DataOut call(DataIn dataIn) {
			RestRequest request = new RestRequest("call?access_token=" + access_token, DataFormat.Json);
			request.AddJsonBody(dataIn);

			//IRestResponse response = client.Post(request);
			//return new JsonDeserializer().Deserialize<DataOut>(response);
			return client.Post<DataOut>(request).Data; // ToDo: Test - schlecht zum Debuggen
		}

		[Obsolete("Idea for generic method")] // ToDo
		public DataOut call(Method method, call-string, DataIn dataIn, Object dataOutType?) { // ToDo: use Method.POST
			RestRequest request = new RestRequest("call?access_token=" + access_token, DataFormat.Json);
			request = new RestRequest(method);
			request.AddJsonBody(dataIn);

			IRestResponse response = client.Execute(request);

			return new JsonDeserializer().Deserialize<DataOut>(response);
		}
	}

--------------------------------------------------------------------------------

byte[] jsonUtf8Bytes;
var options = new JsonSerializerOptions
{
    WriteIndented = true
};
jsonUtf8Bytes = JsonSerializer.SerializeToUtf8Bytes(weatherForecast, options);

--------------------------------------------------------------------------------

//[JsonProperty("something")]



[JsonIgnoreAttribute]



[JsonObject(MemberSerialization.OptIn)]

	[JsonProperty]